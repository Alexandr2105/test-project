package com.kerimovalex.testproject.api;

import com.kerimovalex.testproject.model.DataModel;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public class ServerRequests {
    interface IData {
        @GET("search")
        Observable<DataModel> getData(@QueryMap Map<String, Object> date);
    }
}
