package com.kerimovalex.testproject.model;

import com.google.gson.annotations.SerializedName;

public class ArticleDataModel {
    @SerializedName("id")
    private String id;
    @SerializedName("type")
    private String type;
    @SerializedName("sectionId")
    private String sectionId;
    @SerializedName("sectionName")
    private String sectionName;
    @SerializedName("webPublicationDate")
    private String webPublicationDate;
    @SerializedName("webTitle")
    private String webTitle;
    @SerializedName("webUrl")
    private String webUrl;
    @SerializedName("apiUrl")
    private String apiUrl;
    @SerializedName("isHosted")
    private Boolean isHosted;
    @SerializedName("pillarId")
    private String pillarId;
    @SerializedName("pillarName")
    private String pillarName;
    @SerializedName("fields")
    private FieldsModel fieldsModel;

    public FieldsModel getFieldsModel() {
        return fieldsModel;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getSectionId() {
        return sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public String getWebPublicationDate() {
        return webPublicationDate;
    }

    public String getWebTitle() {
        return webTitle;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public Boolean getHosted() {
        return isHosted;
    }

    public String getPillarId() {
        return pillarId;
    }

    public String getPillarName() {
        return pillarName;
    }
}
