package com.kerimovalex.testproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseModel {
    @SerializedName("status")
    private String status;
    @SerializedName("userTier")
    private String userTier;
    @SerializedName("total")
    private Integer total;
    @SerializedName("startIndex")
    private Integer startIndex;
    @SerializedName("pageSize")
    private Integer pageSize;
    @SerializedName("currentPage")
    private Integer currentPage;
    @SerializedName("pages")
    private Integer pages;
    @SerializedName("orderBy")
    private String orderBy;
    @SerializedName("results")
    private List<ArticleDataModel> results = null;

    public String getStatus() {
        return status;
    }

    public String getUserTier() {
        return userTier;
    }

    public Integer getTotal() {
        return total;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public Integer getPages() {
        return pages;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public List<ArticleDataModel> getResults() {
        return results;
    }
}
