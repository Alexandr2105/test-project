package com.kerimovalex.testproject.model;

import com.google.gson.annotations.SerializedName;

public class DataModel {
    @SerializedName("response")
    private ResponseModel responseModel;

    public ResponseModel getResponse() {
        return responseModel;
    }
}
