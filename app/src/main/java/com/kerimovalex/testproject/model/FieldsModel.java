package com.kerimovalex.testproject.model;

import com.google.gson.annotations.SerializedName;

public class FieldsModel {
    @SerializedName("thumbnail")
    private String thumbnail;

    public String getThumbnail() {
        return thumbnail;
    }
}
