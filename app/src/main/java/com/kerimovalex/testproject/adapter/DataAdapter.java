package com.kerimovalex.testproject.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kerimovalex.test.R;
import com.kerimovalex.testproject.model.ArticleDataModel;
import com.kerimovalex.testproject.tools.DateConverter;


import org.joda.time.DateTime;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private List<ArticleDataModel> dateList;
    private Context context;

    public DataAdapter(List<ArticleDataModel> dateList, Context context) {
        this.dateList = dateList;
        this.context = context;
    }

    public List<ArticleDataModel> getDateList() {
        return dateList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_date, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ArticleDataModel item = dateList.get(position);
        holder.title.setText(item.getWebTitle());
        if (item.getFieldsModel() != null) {
            Glide.with(context).load(item.getFieldsModel().getThumbnail())
                    .apply(RequestOptions.placeholderOf(R.drawable.error_image).error(R.drawable.error_image))
                    .into(holder.imageView);
        } else {
            Glide.with(context).load(R.drawable.error_image).into(holder.imageView);
        }
        DateTime dateTime = new DateTime(item.getWebPublicationDate());
        holder.date.setText(DateConverter.convertDate(dateTime));
        holder.category.setText(item.getSectionName());
    }


    public void updateDataPagination(List<ArticleDataModel> dateList) {
        this.dateList.addAll(dateList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dateList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.image)
        ImageView imageView;
        @BindView(R.id.category)
        TextView category;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }


    }
}
