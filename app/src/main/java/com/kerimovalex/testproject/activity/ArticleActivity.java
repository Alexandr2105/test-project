package com.kerimovalex.testproject.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kerimovalex.test.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArticleActivity extends Activity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView titleToolbar;
    @BindView(R.id.image_article)
    ImageView image;
    @BindView(R.id.article_date)
    TextView date;
    @BindView(R.id.article_category)
    TextView category;
    @BindView(R.id.article_title)
    TextView title;
    private Intent intent;
    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        ButterKnife.bind(this);
        context = this;
        intent = getIntent();
        if (intent != null) {
            category.setText(intent.getStringExtra("Category"));
            date.setText(intent.getStringExtra("Time"));
            title.setText(intent.getStringExtra("Title"));
            Glide.with(context).load(intent.getStringExtra("Image"))
                    .apply(RequestOptions.placeholderOf(R.drawable.error_image).error(R.drawable.error_image))
                    .into(image);
        }
        initToolbar();
    }

    private void initToolbar() {
        titleToolbar.setText("Article");
    }

    @OnClick(R.id.back)
    public void backClick() {
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
