package com.kerimovalex.testproject.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kerimovalex.test.R;
import com.kerimovalex.testproject.adapter.DataAdapter;
import com.kerimovalex.testproject.api.SingletonRest;
import com.kerimovalex.testproject.database.App;
import com.kerimovalex.testproject.database.AppDataBase;
import com.kerimovalex.testproject.database.Article;
import com.kerimovalex.testproject.database.ArticleDao;
import com.kerimovalex.testproject.dialogs.MaterialProgressDialog;
import com.kerimovalex.testproject.errors.Error;
import com.kerimovalex.testproject.interfaces.Constant;
import com.kerimovalex.testproject.listeners.RecyclerItemClickListener;
import com.kerimovalex.testproject.model.ArticleDataModel;
import com.kerimovalex.testproject.model.DataModel;
import com.kerimovalex.testproject.tools.DateConverter;


import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.reactivestreams.Subscriber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements Constant {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.symptom_progressBar)
    ProgressBar progressBar;
    private Map<String, Object> map;
    private DataAdapter dataAdapter;
    private LinearLayoutManager linearLayoutManager;
    private Context context;
    private int currentPage, lastPage, visibleItemCount, totalItemCount, firstVisibleItem;
    private boolean loading = true;
    private Error error;
    private String errorMessage;
    private Disposable disposable;
    private MaterialProgressDialog materialProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        JodaTimeAndroid.init(this);
        ButterKnife.bind(this);
        currentPage = 1;
        error = new Error(MainActivity.this);
        context = this;
        materialProgressDialog = MaterialProgressDialog.getProgressDialog(MainActivity.this);
        disposable = Observable.interval(30, TimeUnit.SECONDS).subscribe(aLong -> {
            getDataPagination();
        });
        getData();

    }

    @SuppressLint("CheckResult")
    private void getData() {
        materialProgressDialog.show();
        map = new HashMap<>();
        map.put("api-key", KEY);
        map.put("page", 1);
        map.put("show-fields", "thumbnail");
        SingletonRest.getInstance()
                .getDataInfo(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::initRecycleView, throwable -> {
                    errorMessage = error.handleError(throwable);
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    materialProgressDialog.dismiss();
                });
    }


    private void initRecycleView(DataModel dataModel) {
        lastPage = dataModel.getResponse().getPages();
        currentPage = dataModel.getResponse().getCurrentPage() + 1;
        dataAdapter = new DataAdapter(dataModel.getResponse().getResults(), context);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(dataAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @SuppressLint("CheckResult")
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                if (loading && (visibleItemCount + firstVisibleItem) >= totalItemCount) {
                    loading = false;
                    if (currentPage <= lastPage) {
                        progressBar.setVisibility(View.VISIBLE);
                        Observable.timer(300, TimeUnit.MILLISECONDS).subscribe(aLong -> {
                            getDataPagination();
                        });
                    }
                }
            }


        });
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context, (view, position) -> {
            ArticleDataModel item = dataAdapter.getDateList().get(position);
            Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
            intent.putExtra("Title", item.getWebTitle());
            if (item.getFieldsModel() != null) {
                intent.putExtra("Image", item.getFieldsModel().getThumbnail());
            } else {
                intent.putExtra("Image", R.drawable.error_image);
            }
            DateTime dateTime = new DateTime(item.getWebPublicationDate());
            intent.putExtra("Time", DateConverter.convertDate(dateTime));
            intent.putExtra("Category", item.getSectionName());
            startActivity(intent);
        }));
        materialProgressDialog.dismiss();
    }

    @SuppressLint("CheckResult")
    private void getDataPagination() {
        map.put("page", currentPage);
        map.put("api-key", KEY);
        map.put("show-fields", "thumbnail");
        SingletonRest.getInstance()
                .getDataInfo(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataModel1 -> {
                    lastPage = dataModel1.getResponse().getPages();
                    currentPage = dataModel1.getResponse().getCurrentPage() + 1;
                    dataAdapter.updateDataPagination(dataModel1.getResponse().getResults());
                    progressBar.setVisibility(View.GONE);
                    loading = true;
                }, throwable -> {
                    errorMessage = error.handleError(throwable);
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    loading = true;
                });

    }

    @Override
    protected void onStop() {
        super.onStop();
        stopClockInternal();
    }

    private void stopClockInternal() {
        if (disposable != null) {
            disposable.dispose();
        }
    }
}
